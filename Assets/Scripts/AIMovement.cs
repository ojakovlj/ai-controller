﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Controls animations, character movement and navigation agent movement.
/// </summary>
public class AIMovement : MonoBehaviour {

    [HideInInspector]
	public NavMeshAgent NavAgent;
    [Tooltip("Distance to NavMeshAgent")]
	public float Distance;
    [Tooltip("AI unit speed")]
	public float Speed = 0;
    [Tooltip("Slow movement speed of the navmesh agent")]
    public float NavMeshAgentLowSpeed = 2f;
    [Tooltip("Fast movement speed of the navmesh agent")]
    public float NavMeshAgentHighSpeed = 4f;

    private Animator animator;
    private AICharacterController controller;
    private Vector3 rotationDestination;

    private void Awake()
	{
        animator = GetComponent<Animator>();
        controller = GetComponent<AICharacterController>();
    }

    private void Start()
    {
        NavAgent = GetComponent<AICharacterController>().NavAgent;
    }

    private void LateUpdate()
	{
		Distance = Vector3.Distance(transform.position, NavAgent.transform.position);

        if(controller.CurrentStateName == AICharacterController.AIState.Attack)
        {
            rotationDestination = new Vector3(controller.target.position.x, 0, controller.target.position.z);
        }
        else
        {
            rotationDestination = NavAgent.transform.position;
        }

        // Compute rotation
        Quaternion lookDir = Quaternion.LookRotation(rotationDestination - transform.position);

        if (Distance > controller.TriggerRanges.StoppingDistance)
        {
            //NavAgent.speed = NavMeshAgentHighSpeed;
            transform.rotation = Quaternion.Lerp(transform.rotation, lookDir, Time.deltaTime * 5);
            Speed = Mathf.Lerp(Speed, 1, Time.deltaTime * 5);
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, transform.rotation, Time.deltaTime * 5);

            //NavAgent.speed = NavMeshAgentLowSpeed;
            Speed = Mathf.Lerp(Speed, 0, Time.deltaTime * 1);
        }

        if (controller.CurrentStateName == AICharacterController.AIState.Chase)
        {
            if (Vector3.Distance(controller.transform.position, controller.target.position) < controller.TriggerRanges.ChaseWalkDistance)
                animator.SetTrigger("StartWalking");
            else 
                animator.SetTrigger("StartRunning");
        }

        animator.SetFloat("Speed", Speed);
    }

    public void StartRunning()
    {
        // Stop punching
        animator.SetInteger("StartPunching", -1);

        animator.SetTrigger("StartRunning");
        NavAgent.speed = NavMeshAgentHighSpeed;
    }

    public void StartWalking()
    {
        // Stop punching
        animator.SetInteger("StartPunching", -1);

        animator.SetTrigger("StartWalking");
        if (NavAgent == null)
            Start();
        NavAgent.speed = NavMeshAgentLowSpeed;
    }

    public void StartPunching()
    {
        //Random rnd = new System.Random();
        //var attackSequence = Enumerable.Range(1, 3).OrderBy(r => rnd.Next(1,3)).ToArray();
        animator.ResetTrigger("StartWalking");
        animator.ResetTrigger("StartRunning");

        int anim = Random.Range(0, 3);
        animator.SetInteger("StartPunching", anim);
        NavAgent.speed = NavMeshAgentHighSpeed;
    }
}
