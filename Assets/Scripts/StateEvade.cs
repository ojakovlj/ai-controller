﻿using System.Collections.Generic;
using UnityEngine;

public class StateEvade : State
{
    private string tag;

    public StateEvade(AICharacterController charController) : base(charController)
    {
    }

    public override void InitState(AICharacterController charController)
    {
        // Set animation and navMeshAgent speed
        charController.MovementController.StartRunning();
        tag = charController.tag;
    }

    public override void UpdateState(AICharacterController charController)
    {
        var nearestEnemy = tag == "Friendly" ? 
            CharacterList.GetClosestEnemies(charController.transform.position, charController.DetectionRanges.ForwardDetectionRange, 1) :
            CharacterList.GetClosestFriendlies(charController.transform.position, charController.DetectionRanges.ForwardDetectionRange, 1);

        if (nearestEnemy.Count == 0 && tag == "Enemy")
        {
            nearestEnemy = new List<GameObject>();
            nearestEnemy.Add(charController.player);
        }

        // If character ran away from threat
        if (nearestEnemy.Count == 0 || Vector3.Distance(nearestEnemy[0].transform.position, charController.transform.position) > 
            charController.TriggerRanges.EvadeSuccessfulDistance)
        {
            // Switch back to patrol state
            charController.ChangeState(AICharacterController.AIState.Patrol);
        }
        else { 
            // Run away from nearest enemy
            Vector3 runawayDirection = charController.transform.position - nearestEnemy[0].transform.position;

            Debug.DrawLine(charController.transform.position, charController.transform.position + runawayDirection.normalized * 10f, Color.yellow, 5f);
            charController.NavAgent.SetDestination(charController.transform.position + runawayDirection.normalized *10f);
        }
    }

}