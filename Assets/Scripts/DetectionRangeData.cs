﻿using UnityEngine;

[CreateAssetMenu(menuName = "DetectionRangeData")]
public class DetectionRangeData : ScriptableObject
{
    [Tooltip("Range at which opponents are unconditionally detected")]
    public float AssuredDetectionRange;
    [Tooltip("Range at which opponents are detected if in front")]
    public float ForwardDetectionRange;

}