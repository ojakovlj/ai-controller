﻿using UnityEngine;

[CreateAssetMenu(menuName = "TriggerRangeData")]
public class TriggerRangeData : ScriptableObject {

    [Tooltip("Distance at which the AI character stops running from an enemy " +
        "and reverts to the patrol state")]
    public float EvadeSuccessfulDistance = 10f;
    [Tooltip("Distance at which character changes from Chase to Attack (melee)")]
    public float ChasingToAttackDistance = 2f;
    [Tooltip("Distance at which the current destination has been reached when " +
        "patrolling.")]
    public float PatrolDestinationReachedRange = 1f;
    [Tooltip("The range of the melee attack. If target is outside of this range," +
        "the AI switches to Chase")]
    public float MeleeAttackRange = 2f;
    [Tooltip("The distance at which, when chasing, the agent stop running and starts " +
        "walking to slow down.")]
    public float ChaseWalkDistance = 3f;
    [Tooltip("The range at which the speed of the agent is slowly reduced")]
    public float StoppingDistance = .5f;

}
