﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

public class CharacterList : MonoBehaviour
{
    public static GameObject Player;
    public static List<GameObject> FriendlyCharacters;
    public static List<GameObject> EnemyCharacters;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        GetExistingObjects();
    }

    /// <summary>
    /// Gets a list of <paramref name="num"/> friendly characters within <paramref name="range"/>
    /// from a given <paramref name="position"/>. If 0 is passed as "num", all are returned.
    /// The list is sorted so that the nearest target is first in the list.
    /// </summary>
    public static List<GameObject> GetClosestFriendlies(Vector3 position, float range, int num = 0)
    {
        return GetClosest(FriendlyCharacters, position, range, num);
    }

    /// <summary>
    /// Gets a list of <paramref name="num"/> enemy characters within <paramref name="range"/>
    /// from a given <paramref name="position"/>. If 0 is passed as "num", all are returned.
    /// /// The list is sorted so that the nearest target is first in the list.
    /// </summary>
    public static List<GameObject> GetClosestEnemies(Vector3 position, float range, int num = 0)
    {
        return GetClosest(EnemyCharacters, position, range, num);
    }

    private static void GetExistingObjects()
    {
        FriendlyCharacters = new List<GameObject>();
        EnemyCharacters = new List<GameObject>();

        // Find all pre-existing level entities
        Player = GameObject.FindGameObjectWithTag("Player");
        FriendlyCharacters.AddRange(GameObject.FindGameObjectsWithTag("Friendly"));
        EnemyCharacters.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));

        Debug.Log("CharacterList found " + FriendlyCharacters.Count + " friendly characters and "
            + EnemyCharacters.Count + " enemies.");
    }

    private static List<GameObject> GetClosest(List<GameObject> charList, Vector3 position, float range, int num)
    {
        Dictionary<GameObject, float> charDistances = new Dictionary<GameObject, float>();
        if (charList == null)
            return new List<GameObject>();

        float distance;

        int counter = 0;
        // Find <num> of friendly characters in <range> from <position>
        foreach (var character in charList)
        {
            distance = Vector3.Distance(position, character.transform.position);
            if (distance < range)
            {
                charDistances.Add(character, distance);
                counter++;
                if (num != 0 && counter >= num)
                    break;
            }
        }

        // Sort by distance to get closest targets
        List<KeyValuePair<GameObject, float>> closestSorted = charDistances.ToList();

        closestSorted.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

        List<GameObject> closestContacts = new List<GameObject>();

        foreach (KeyValuePair<GameObject, float> pair in closestSorted)
        {
            closestContacts.Add(pair.Key);
        }

        return closestContacts;
    }


}
