﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State {

    public State(AICharacterController charController)
    {
        InitState(charController);
    }

    /// <summary>
    /// Should be called when the state is activated (instantiated).
    /// </summary>
    /// <param name="charController">Character controller which activated this state</param>
    public abstract void InitState(AICharacterController charController);

    /// <summary>
    /// Should be called in every Update function of the AI Character Controller.
    /// </summary>
    /// <param name="charController">Character controller which activated this state</param>
    public abstract void UpdateState(AICharacterController charController);

    /// <summary>
    /// Should be called just before the state is changed to another.
    /// </summary>
    /// <param name="charController">Character controller which activated this state</param>
    //public abstract void EndState(AICharacterController charController);

}
