﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateChase : State
{
    public StateChase(AICharacterController charController) : base(charController)
    {
    }

    public override void InitState(AICharacterController charController)
    {
        // Set animation and navMeshAgent speed
        charController.MovementController.StartRunning();
    }

    public override void UpdateState(AICharacterController charController)
    {
        if (charController.target != null)
        {
            Debug.DrawLine(charController.transform.position, charController.target.position, Color.red);

            // Move to target
            charController.NavAgent.SetDestination(charController.target.transform.position);
            //charController.MovementController.StartRunning();

            // Attack if close to target
            if (Vector3.Distance(charController.target.position, charController.transform.position) < 
                charController.TriggerRanges.ChasingToAttackDistance)
            {
                charController.ChangeState(AICharacterController.AIState.Attack);
            }
            if (Vector3.Distance(charController.target.position, charController.NavAgent.transform.position) < 
                charController.TriggerRanges.ChasingToAttackDistance)
            {
                charController.NavAgent.SetDestination(charController.NavAgent.transform.position);
            }
        }
        else
        {
            // Switch back to patrol state (target lost)
            charController.ChangeState(AICharacterController.AIState.Patrol);
        }
    }
}
