﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StatePatrol : State {

    // Idle timer
    private float idleTimer, checkEnemyTimer;
    // Character will idle for a random duration when each waypoint is reached
    private bool isIdling = false;
    private AICharacterController cc;

    public StatePatrol(AICharacterController charController) : base(charController)
    {
        cc = charController;
    }

    public override void InitState(AICharacterController charController)
    {
        idleTimer = 0;
        checkEnemyTimer = charController.TargetUpdateInterval;

        isIdling = false;
        charController.MovementController.StartWalking();
    }

    public override void UpdateState(AICharacterController charController)
    {
        if (isIdling) { 
            idleTimer -= Time.deltaTime;
            if (idleTimer < 0) {
                // Idle is done, set a waypoint and move to it
                isIdling = false;
                charController.target = GetNextWaypoint(charController);
            }
        }
        if (!isIdling) { 
            if (charController.target == null || Vector3.Distance(charController.transform.position, charController.target.position) 
                < charController.TriggerRanges.PatrolDestinationReachedRange)
            {
                // Change to idle anim and hold it for several seconds (1-5 sec)
                idleTimer = Random.Range(1, 5);
                isIdling = true;
            }
            else
            {
                // Move towards destination (waypoint)
                charController.NavAgent.SetDestination(charController.target.transform.position);
            }
        }

        checkEnemyTimer -= Time.deltaTime;
        if(checkEnemyTimer < 0)
        {
            checkEnemyTimer = charController.TargetUpdateInterval;
            CheckForEnemies();
        }

    }

    /// <summary>
    /// Gets a random waypoint, ensuring it is not the same as the previous target.
    /// </summary>
    /// <returns>The new Target (a random waypoint)</returns>
    private Transform GetNextWaypoint(AICharacterController charController)
    {
        Transform nextWaypoint = charController.target;

        while (nextWaypoint == charController.target)
            nextWaypoint = charController.patrolWaypoints[Random.Range(0, charController.patrolWaypoints.Length)].transform;

        charController.target = nextWaypoint;
        return nextWaypoint;
    }

    /// <summary>
    /// Checks for hostile contacts in the vicinity based on the detection ranges of this character.
    /// The most suitable/important enemy is marked as the target.
    /// </summary>
    private void CheckForEnemies()
    {
        Transform enemyTarget = null;
        RaycastHit hit;
        Vector3 viewVector;
        Vector3 yOffset = AICharacterController.offset;
        float distance = Vector3.Distance(cc.transform.position, cc.player.transform.position);

        // Check player target
        if (cc.tag == "Enemy" && IsEnemyReachable(cc.player.transform))
        {
            viewVector = cc.player.transform.position - (cc.transform.position + yOffset);
            // Check if view is obstructed
            if (Physics.Raycast(cc.transform.position + yOffset, viewVector, out hit, cc.DetectionRanges.ForwardDetectionRange))
            {
                Debug.DrawLine(cc.transform.position + yOffset, hit.point, Color.blue, 5f);
                if (hit.transform.tag == "Player")
                {
                    // Check range
                    if (distance < cc.DetectionRanges.AssuredDetectionRange)
                    {
                        enemyTarget = cc.player.transform;
                    }
                    else if (IsInForwardViewArc(cc.player.transform) && distance < cc.DetectionRanges.ForwardDetectionRange)
                    {
                        enemyTarget = cc.player.transform;
                    }
                }
            }
        }

        // Get enemies in range
        var enemies = (cc.tag == "Enemy") ?
            CharacterList.GetClosestFriendlies(cc.transform.position, cc.DetectionRanges.ForwardDetectionRange) :
            CharacterList.GetClosestEnemies(cc.transform.position, cc.DetectionRanges.ForwardDetectionRange);


        foreach (var enemy in enemies)
        {
            // Check if enemy is reachable
            if (!IsEnemyReachable(enemy.transform))
                continue;

            // Check if view is obstructed
            viewVector = enemy.transform.position - (cc.transform.position + yOffset);
            if (Physics.Raycast(cc.transform.position + yOffset, viewVector, out hit, cc.DetectionRanges.ForwardDetectionRange))
            {
                Debug.DrawLine(cc.transform.position + yOffset, hit.point, Color.blue, 5f);
                if (hit.transform.tag != GetEnemyTag())
                    continue;   // Raycast is blocked by another object
            }
            else  // Enemy is out of range
                continue;

            // Check distance
            distance = Vector3.Distance(cc.transform.position, enemy.transform.position);
            if (enemyTarget == null)
            {
                if (distance < cc.DetectionRanges.AssuredDetectionRange)
                    enemyTarget = enemy.transform;
                else if (IsInForwardViewArc(cc.player.transform) && distance < cc.DetectionRanges.ForwardDetectionRange)
                    enemyTarget = enemy.transform;
            }
            else if (distance < Vector3.Distance(enemyTarget.position, cc.transform.position))
            {
                if (distance < cc.DetectionRanges.AssuredDetectionRange)
                    enemyTarget = enemy.transform;
                else if (IsInForwardViewArc(cc.player.transform) && distance < cc.DetectionRanges.ForwardDetectionRange)
                    enemyTarget = enemy.transform;
            }
        }

        if (enemyTarget != null)
        {
            // Switch to chase mode
            cc.target = enemyTarget;

            if (cc.IsAggressive)
                cc.ChangeState(AICharacterController.AIState.Chase);
            else
                cc.ChangeState(AICharacterController.AIState.Evade);
        }
    }

    /// <summary>
    /// Determines whether a character is within the forward view arc defined by 
    /// the 'ForwardViewArcAngle' variable.
    /// </summary>
    /// <param name="other">Target to check</param>
    /// <returns>True if within forward view arc</returns>
    private bool IsInForwardViewArc(Transform other)
    {
        if (Mathf.Abs(Vector3.Angle(other.position - cc.transform.position, cc.transform.forward)) < cc.ForwardViewArcAngle / 2)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Tells whether the enemy position is reachable (same NavMesh).
    /// </summary>
    /// <param name="other">Transform of the enemy target</param>
    /// <returns>True if enemy is reachable by NavMesh</returns>
    private bool IsEnemyReachable(Transform other)
    {
        NavMeshPath path = new NavMeshPath();
        return cc.NavAgent.CalculatePath(other.position, path);
    }

    private string GetEnemyTag()
    {
        return cc.tag == "Enemy" ? "Friendly" : "Enemy";
    }

}
