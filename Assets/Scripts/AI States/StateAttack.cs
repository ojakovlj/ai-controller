﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAttack : State
{
    public StateAttack(AICharacterController charController) : base(charController)
    {
    }

    public override void InitState(AICharacterController charController)
    {
        charController.MovementController.StartPunching();
    }

    public override void UpdateState(AICharacterController charController)
    {
        if (charController.target != null)
        {
            if (Vector3.Distance(charController.target.position, charController.transform.position) > 
                charController.TriggerRanges.MeleeAttackRange)
            {
                charController.ChangeState(AICharacterController.AIState.Chase);
            }
            else
            {
                charController.MovementController.StartPunching();
            }
        }
        else
        {
            charController.ChangeState(AICharacterController.AIState.Patrol);
        }
    }

}
