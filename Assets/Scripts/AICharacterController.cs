﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Controls character states and transitions.
/// </summary>
public class AICharacterController : MonoBehaviour {

    public enum AIState
    {
        Chase,          // Chase an enemy target to get in melee range
        Patrol,         // Move between random waypoints
        Attack,         // Attack when in proximity of enemy
        AttackChase,    // Attack while chasing (ranged attack)
        Evade           // Run away from closest enemy (TODO)
    }

    [Header("References")]
    [Tooltip("Navigation agent which the AI follows, for pathfinding")]
    public GameObject NavAgentPrefab;
    [Tooltip("Animation and movement controller")]
    public AIMovement MovementController;

    [Header("Properties")]
    [Tooltip("AI state update interval in seconds")]
    public float TargetUpdateInterval = 2f;
    [Tooltip("Data holder for view ranges")]
    public DetectionRangeData DetectionRanges;
    [Tooltip("Data holder for trigger distances used for state transitions")]
    public TriggerRangeData TriggerRanges;
    [Tooltip("Total angle of the forward view arc in degrees")]
    public float ForwardViewArcAngle = 160;
    [Tooltip("Is this AI character agressive (will attack) or passive (will evade)")]
    public bool IsAggressive = true;
    [Tooltip("Range of character's ranged attack. If zero, character cannot " +
        "perform ranged attacks.")]
    public float MaxAttackRange = 15f;

    [HideInInspector]
    public NavMeshAgent NavAgent;
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public GameObject[] patrolWaypoints;
    [HideInInspector]
    public GameObject player;
    public static Vector3 offset = Vector3.up * 1.5f;

    private State currentState;
    public AIState CurrentStateName
    {
        get { return currentStateName; }
    }
    private AIState currentStateName;
    private TextMesh textMesh;
    

    void Awake()
    {
        NavAgent = GameObject.Instantiate(NavAgentPrefab, transform.position, Quaternion.identity).GetComponent<NavMeshAgent>();
        patrolWaypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        player = GameObject.FindGameObjectWithTag("Player");
        textMesh = GetComponent<TextMesh>();
    }

    void Update ()
    {
        if (currentState == null)
            ChangeState(AIState.Patrol);

        currentState.UpdateState(this);
    }

    public void ChangeState(AIState newState)
    {
        Debug.Log(gameObject.name+ " is changing state to " + newState);
        switch (newState)
        {
            case AIState.Patrol:
                if (currentStateName == AIState.Patrol)
                    return;
                currentState = new StatePatrol(this);
                currentStateName = AIState.Patrol;
                textMesh.text = "State: Patrol";
                break;
            case AIState.Attack:
                if (currentStateName == AIState.Attack)
                    return;
                currentState = new StateAttack(this);
                currentStateName = AIState.Attack;
                textMesh.text = "State: Attack";
                break;
            case AIState.Chase:
                if (currentStateName == AIState.Chase)
                    return;
                currentState = new StateChase(this);
                currentStateName = AIState.Chase;
                textMesh.text = "State: Chase";
                break;
            case AIState.Evade:
                if (currentStateName == AIState.Evade)
                    return;
                currentState = new StateEvade(this);
                currentStateName = AIState.Evade;
                textMesh.text = "State: Evade";
                break;
        }
    }
   

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, DetectionRanges.AssuredDetectionRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, DetectionRanges.ForwardDetectionRange);
    }

}
